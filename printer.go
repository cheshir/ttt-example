package main

import (
	"fmt"
	"strings"
)

const (
	colDelimiter = '|'     // Используется для рисования колонок.
	rowDelimiter = "–+–+–" // Используется для рисования горизонтальных линий игрового поля.
)

// Определяем символы, которыми будем отображать состояния ячеек игрового поля.
var cellStatePrintableMap = map[cellState]byte{
	emptyCell:     ' ',
	playersCell:   'x',
	computersCell: 'o',
}

// Наш печататель в терминал. Скрывает логику, связанную с выводом поля в терминал.
type TerminalPrinter struct{}

func NewTerminalPrinter() *TerminalPrinter {
	return &TerminalPrinter{}
}

// Выводит в терминал игровое поле.
func (p *TerminalPrinter) PrintBoard(board [][]cellState) error {
	var builder strings.Builder // Буфер для частей строки, где будет храниться отрисованное игровое поле.

	// Escape-последовательность, которая отдает команду термиралу очистить содержимое окна.
	// x1B – это код ESC (escape) в таблице ASCII в 16-тиричной системе исчесления.
	// @link https://ru.wikipedia.org/wiki/Управляющая_последовательность
	// @link https://ru.wikipedia.org/wiki/Управляющие_последовательности_ANSI
	builder.Write([]byte{'\x1B', 'c'})

	// Итерируемся по строкам игрового поля.
	for rowindex, row := range board {
		// Записываем в буфер отображение первой ячейки каждого ряда.
		builder.WriteByte(cellStatePrintableMap[row[0]])

		// Итерируемся по оставшимся в ряду ячейкам (без первой).
		for _, cell := range row[1:] {
			// Добавляем в буфер символ колонки, разделяя таким образом соседние ячейки по горизонтале.
			builder.WriteByte(colDelimiter)
			// Записываем в буфер символ ячейки.
			builder.WriteByte(cellStatePrintableMap[cell])
		}
		// В конце ряда игрового поля добавляем перенос строки.
		builder.WriteByte('\n')

		// Если это не последний ряд, тогда записываем в буфер разделитель строк.
		if rowindex < len(board)-1 {
			builder.WriteString(rowDelimiter)
			builder.WriteByte('\n')
		}
	}

	// Возвращаем накопленные кусочки в буфере в виде строки.
	fmt.Println(builder.String())

	return nil
}
