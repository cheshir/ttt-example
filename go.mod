module gitlab.com/cheshir/ttt-example

go 1.14

require (
	github.com/cheshir/tttai v1.0.0
	github.com/stretchr/testify v1.5.1
)
