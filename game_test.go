package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// Тестируем метод Board структуры TestTicTacToe.
func TestTicTacToe_Board(t *testing.T) {
	// Список тестовых сценариев и их параметры.
	// Подход называется табличные тесты:
	// @link https://разработка-программ.рф/заметки/тесты-в-go/
	// @link https://github.com/golang/go/wiki/TableDrivenTests
	tt := []struct {
		name  string
		game  *TicTacToe
		board [][]cellState
	}{
		{
			name: "empty board",
			game: NewTicTacToe(2),
			board: [][]cellState{
				{emptyCell, emptyCell},
				{emptyCell, emptyCell},
			},
		},
		{
			name: "right bottom corner",
			game: func() *TicTacToe {
				game := NewTicTacToe(2)
				_ = game.Move(1, 1, playersCell)

				return game
			}(),
			board: [][]cellState{
				{emptyCell, emptyCell},
				{emptyCell, playersCell},
			},
		},
		{
			name: "right bottom corner",
			game: func() *TicTacToe {
				game := NewTicTacToe(2)
				_ = game.Move(0, 0, playersCell)
				_ = game.Move(0, 1, computersCell)
				_ = game.Move(1, 0, computersCell)
				_ = game.Move(1, 1, playersCell)

				return game
			}(),
			board: [][]cellState{
				{playersCell, computersCell},
				{computersCell, playersCell},
			},
		},
	}

	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			board := test.game.Board()
			assert.Equal(t, test.board, board)
		})
	}
}

func TestTicTacToe_Result(t *testing.T) {
	tt := []struct {
		name   string
		game   *TicTacToe
		result gameResult
	}{
		{
			name:   "fresh board",
			game:   NewTicTacToe(3),
			result: undefined,
		},
		{
			name: "horizontal",
			game: func() *TicTacToe {
				game := NewTicTacToe(3)
				game.freeCells = 3
				game.board = []cellState{
					emptyCell, playersCell, emptyCell,
					computersCell, computersCell, computersCell,
					emptyCell, playersCell, playersCell,
				}

				return game
			}(),
			result: computerWin,
		},
		{
			name: "vertical",
			game: func() *TicTacToe {
				game := NewTicTacToe(3)
				game.freeCells = 3
				game.board = []cellState{
					emptyCell, playersCell, emptyCell,
					computersCell, playersCell, computersCell,
					emptyCell, playersCell, computersCell,
				}

				return game
			}(),
			result: playerWin,
		},
		{
			name: "left diagonal",
			game: func() *TicTacToe {
				game := NewTicTacToe(3)
				game.freeCells = 1
				game.board = []cellState{
					playersCell, computersCell, emptyCell,
					computersCell, playersCell, computersCell,
					playersCell, computersCell, playersCell,
				}

				return game
			}(),
			result: playerWin,
		},
		{
			name: "right diagonal",
			game: func() *TicTacToe {
				game := NewTicTacToe(3)
				game.freeCells = 1
				game.board = []cellState{
					emptyCell, computersCell, playersCell,
					computersCell, playersCell, computersCell,
					playersCell, computersCell, playersCell,
				}

				return game
			}(),
			result: playerWin,
		},
		{
			name: "draw",
			game: func() *TicTacToe {
				game := NewTicTacToe(3)
				game.freeCells = 0
				game.board = []cellState{
					computersCell, playersCell, computersCell,
					playersCell, computersCell, computersCell,
					playersCell, computersCell, playersCell,
				}

				return game
			}(),
			result: draw,
		},
	}

	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			result := test.game.Result()
			assert.Equal(t, test.result, result)
		})
	}
}

func TestTicTacToe_Move(t *testing.T) {
	game := NewTicTacToe(3)
	err := game.Move(0, 0, playersCell)
	require.NoError(t, err)
	assert.Equal(t, playersCell, game.board[0])

	err = game.Move(10, 10, computersCell)
	require.Errorf(t, err, "moving out of the board by index: 40")

	err = game.Move(0, 0, playersCell)
	require.Errorf(t, err, "cell is already occupied")
}
