package main

import (
	"fmt"
)

// Создаем тип для результата игры.
type gameResult uint8

// Задаем возможные исходы игры.
const (
	// Значение по-умолчанию для типа gameResult.
	undefined gameResult = iota
	// Прочие исходы.
	draw
	playerWin
	computerWin
)

// Реализация интерфейса fmt.Stringer с целью выводить на печать пользователю исход игры в понятном ему виде.
func (r gameResult) String() string {
	// Сравнимаем r со значениями указанными в case. Если найдено совпадение – выполняем блок кода в case.
	switch r {
	case draw:
		return "Draw"
	case playerWin:
		return "Player Wins"
	case computerWin:
		return "Computer Wins"
	default: // Выполняется для случаев, когда не найдено совпадение ни с одним case.
		return "Undefined"
	}
}

// Создаем тип, описывающий состояние ячейки игрового поля.
type cellState uint8

// Объявляем возможные состояния ячеек.
const (
	// Значение по-умолчанию для ячейки игрового поля.
	emptyCell cellState = iota
	playersCell
	computersCell
)

// Создаем новый тип, который будет описывать нашу игру.
type TicTacToe struct {
	dimension int         // Размерность игрового поля. Так как поле квадратное, то достаточно указать значение одной стороны.
	board     []cellState // Хранит состояние игрового поля.
	freeCells int         // Счетчик свободных ячеек. Нужен для определения ничьей.
}

// Конструктор нашей игры.
// Принимает желаемую размерность игрового поля. Для классических крестиков-ноликов dimension == 3.
func NewTicTacToe(dimension int) *TicTacToe {
	cellsCount := dimension * dimension // Вычисляем количество ячеек игрового поля.

	// Инициализируем и возвращаем объект игры.
	return &TicTacToe{
		board:     make([]cellState, cellsCount), // Создаем игровое поле.
		dimension: dimension,
		freeCells: cellsCount,
	}
}

// Метод возвращает состояние доски в виде матрицы (таблицы).
// Преобразование в матрицу нужно для удобной работы с доской другими компонентами.
func (t *TicTacToe) Board() [][]cellState {
	// Создаем матрицу игрового поля.
	board := make([][]cellState, t.dimension)

	// Перебираем ячейки игрового поля.
	for i, cell := range t.board {
		rowIndex := i / t.dimension                     // Вычисляем индекс (номер) строки.
		board[rowIndex] = append(board[rowIndex], cell) // Добавляем в один из рядов матрицы ячейку.
	}

	return board
}

// В случае если по переданным координатам находится пустая ячейка – заменяют ее на переданное значение.
// В противном случае возвращает ошибку.
func (t *TicTacToe) Move(x, y int, state cellState) error {
	// Проверям, что координаты ячейки находятся в пределах игровой доски.
	if x >= t.dimension || y >= t.dimension {
		return fmt.Errorf("address must be lower than %d", t.dimension)
	}

	// Вычисляем индекс ячейки из номера ряда и колонки (x и y соответственно).
	index := x*t.dimension + y // if x = 1 and y = 1; index := x * 3 + y = 1 * 3 + 1 = 3 + 1 = 4
	current := t.board[index]  // Смотрим какой статус ячейки находится по переданным координатам.

	// Если ячейка не пустая, ... значит она занята! а согласно правил, мы не может дважды ходить в одну ячейку.
	if current != emptyCell {
		return fmt.Errorf("cell is already occupied")
	}

	// Если все окей, то меняем состояние ячейки с пустой на желаемую.
	t.board[index] = state
	// Уменьшаем счетчик свободных ячеек.
	t.freeCells--

	return nil
}

// Вычисляем результат игры.
func (t *TicTacToe) Result() gameResult {
	// Список функций, которые могут найти победителя или ничью.
	finders := []func() gameResult{
		t.findWinnerHorizontal,
		t.findWinnerVertical,
		t.findWinnerLeftDiagonal,
		t.findWinnerRightDiagonal,
		t.findDraw,
	}

	// По очереди пытаемся найти финальный результат игры используя функции поиска.
	for _, find := range finders {
		// Возвращаем финальный результат игры если нашли его.
		if winner := find(); winner != undefined {
			return winner
		}
	}

	return undefined
}

// Функция поиска победителя в рядах.
// Возвращает победителя если все ячейки в одном ряду не пустые и равны друг другу.
func (t *TicTacToe) findWinnerHorizontal() gameResult {
	// Итерируемся по-строчно.
	for rowIndex := 0; rowIndex < len(t.board); rowIndex += t.dimension {
		// Собственно, сам поиск победителя.
		result := t.findMatch(rowIndex, // С какого элемента начнем поиск.
			// Условие перебора. Проверяем, что не вышли за границы поля.
			func(index int) bool {
				return index < rowIndex+t.dimension
			},
			// Возвращает следующий элемент последовательности.
			func(index int) int {
				return index + 1
			},
		)

		// Если нашли победителя – возвращаем его.
		if result != undefined {
			return result
		}
	}

	// Не нашли победителя.
	return undefined
}

// Ищем победителя в колонках.
func (t *TicTacToe) findWinnerVertical() gameResult {
	// Перебираем колонки.
	for colIndex := 0; colIndex < t.dimension; colIndex++ {
		result := t.findMatch(colIndex,
			func(index int) bool {
				return index < len(t.board)
			},
			func(index int) int {
				return index + t.dimension
			},
		)

		if result != undefined {
			return result
		}
	}

	return undefined
}

// Агалог findWinnerHorizontal.
func (t *TicTacToe) findWinnerHorizontalStraightforward() gameResult {
	for rowIndex := 0; rowIndex < len(t.board); rowIndex += t.dimension {
		cellsAreEqual := true
		first := t.board[rowIndex]

		for i := rowIndex + 1; i < rowIndex+t.dimension; i++ {
			if t.board[i] != first {
				cellsAreEqual = false
				break
			}
		}

		if cellsAreEqual && first != emptyCell {
			return cellStateToWinner[first]
		}
	}

	return undefined
}

// Аналог findWinnerVertical.
func (t *TicTacToe) findWinnerVerticalStraightforward() gameResult {
	for colIndex := 0; colIndex < t.dimension; colIndex++ {
		cellsAreEqual := true
		first := t.board[colIndex]

		for i := colIndex + t.dimension; i < len(t.board); i += t.dimension {
			if t.board[i] != first {
				cellsAreEqual = false
				break
			}
		}

		if cellsAreEqual && first != emptyCell {
			return cellStateToWinner[first]
		}
	}

	return undefined
}

// Ищем победителя в диагонале от верхнего левого угла до нижнего правого.
func (t *TicTacToe) findWinnerLeftDiagonal() gameResult {
	return t.findMatch(0,
		func(index int) bool {
			return index < len(t.board)
		},
		func(currentIndex int) int {
			return currentIndex + t.dimension + 1
		},
	)
}

// Ищем победителя в диагонале от верхнего правого угла к нижнему левому.
func (t *TicTacToe) findWinnerRightDiagonal() gameResult {
	return t.findMatch(t.dimension-1,
		func(index int) bool {
			return index < len(t.board)
		},
		func(currentIndex int) int {
			return currentIndex + t.dimension - 1
		},
	)
}

// Ищет победителя в одной строке.
// Для этого принимает индекс элемента, с которого начнется поиск,
// функцию ограничивающую длинну последовательности (как долго нам надо перебирать элементы)
// и функцию получения следующего элемента.
func (t *TicTacToe) findMatch(startIndex int, condition func(currentIndex int) bool, nextIndex func(currentIndex int) int) gameResult {
	// Переменная, хранящая статус равны ли ячейки последовательности друг другу.
	cellsAreEqual := true

	// Двигаемся по последовательности, начиная со второго элемента.
	// На каждой итерации проверяем текущий элемент с первым.
	// Если все ячейки будут равны первой, значит они равны друг другу.
	for i := nextIndex(startIndex); condition(i); i = nextIndex(i) {
		// Сравниваем текущую ячейку с первой.
		// Если они не равны – нет смысла искать дальше. Меняем наш флаг и выходим из цикла.
		if t.board[startIndex] != t.board[i] {
			cellsAreEqual = false
			break
		}
	}

	// Если все ячейки равны И они не являются пустыми, значит мы нашли победителя.
	if cellsAreEqual && t.board[startIndex] != emptyCell {
		// Конвертируем состояние ячейки в победителя и возвращаем его.
		return cellStateToWinner[t.board[startIndex]]
	}

	// Не нашли победителя.
	return undefined
}

// Мапим состояние ячейки на победителя. Нужно для функции findMatch.
var cellStateToWinner = map[cellState]gameResult{
	emptyCell:     undefined,
	playersCell:   playerWin,
	computersCell: computerWin,
}

// Определяем, а не ничья ли у нас на поле.
func (t *TicTacToe) findDraw() gameResult {
	if t.isDraw() {
		return draw
	}

	return undefined
}

// Если пустых ячеек нет, т.е. некуда ходить – значит ничья. Вот такое допущение.
func (t *TicTacToe) isDraw() bool {
	return t.freeCells == 0
}
