package main

import (
	"fmt"

	"github.com/cheshir/tttai"
)

// Избегаем "магических чисел" в коде. Даем нашей числовой константе имя.
// Подробнее:
// @link https://khasang.io/courses/cleancode/lectures/4364519
// @link http://www.8bytes.net/2018/04/18/antipattern-1-magicheskoe-chislo/
const dimension = 3

func main() {
	fmt.Println("Welcome to the terminal tic tac toe game.")

	// Запускаем раунд игры.
	runRound()
}

func runRound() {
	// Инициализируем нужные нам для игры структуры (создаем объекты).
	game := NewTicTacToe(dimension)
	ai := tttai.NewAI()
	printer := NewTerminalPrinter()

	// Запускаем цикл нашей игры.
	// Так как цикл бесконечный, нужно предусмотреть условия выхода из него, чтоб программа не залипла.
	// В нашем случае, мы выходим из цикла (и из всей функции) благодаря return'ам.
	for {
		// В начале каждого игрового цикла мы печатаем наше игровое поле.
		if err := printer.PrintBoard(game.Board()); err != nil {
			// В случае ошибки выводим ее пользователю и выходим и заканчиваем игровой раунд.
			// Дальше эту конструкцию комментировать не буду.
			fmt.Printf("Unexpected error: %v\n", err)
			return
		}

		// Раньше на этом месте была метка и инструкция goto.
		// Теперь тут явный флаг (булевая переменная) и цикл.
		var playerMadeTurn bool

		// Просим пользователя ввести координаты его хода, пока он не введет легальные значения.
		for !playerMadeTurn {
			// Получаем пользовательский ввод.
			x, y, err := userInput()
			if err != nil {
				fmt.Printf("Unexpected error: %v\n", err)
				return
			}

			// Пробуем поставить "фигурку" пользователя на переданные им координаты.
			if err := game.Move(x, y, playersCell); err != nil {
				fmt.Printf("Failed make the move: %v. Lets try again.\n", err)
				// Вот тут интересно! Если мы не смогли походить, то нужно вывести игроку ошибку
				// и попросить походить еще раз.
				// Для этого мы форсируем переход на новую итерацию цикла с помощью инструкции continue.
				// Мы прерываем выполнение цикла и переходим к следующей итерации.
				continue
			}

			// Меняем наш флаг, чтоб выйти из цикла.
			// Мы могли явно указать break для выхода из цикла, но с флагом читабельнее. Больше контекста для читателя.
			playerMadeTurn = true
		}

		// Смотрим не закончилась ли игра после хода игрока.
		if result := game.Result(); result != undefined {
			fmt.Printf("The game is finished. %v", result)
			return
		}

		// Печатаем состояние доски после хода игрока.
		if err := printer.PrintBoard(game.Board()); err != nil {
			fmt.Printf("Unexpected error: %v\n", err)
			return
		}

		// Генерируем ход компьютера.
		x, y, err := ai.Move(convertBoardToAIFormat(game.Board()))
		if err != nil {
			// Нет смысла пробовать походить снова. Мы расчитываем, что если комп не смог придумать ход и вернул ошибку,
			// то что-то в серьез пошло не так.
			fmt.Printf("AI failed to generate move: %v\n", err)
			return
		}

		// Регистрируем ход компьютера в игре.
		if err := game.Move(x, y, computersCell); err != nil {
			fmt.Printf("Failed make the move: %v\n", err)
			return
		}

		// Проверяем не закончилась ли игра после хода компьютера.
		if result := game.Result(); result != undefined {
			fmt.Printf("The game is finished. %v", result)
			return
		}

		// Итерация цикла закончилась, значит игра не закончена и мы переходим в начало цикла к новой итерации.
	}
}

// Получает координаты игрового поля, куда хочет походить игрок.
func userInput() (int, int, error) {
	fmt.Print("Your move. Specify row number: ")
	var x, y int           // Объявляем переменные, куда будем записывать пользовательский ввод.
	_, err := fmt.Scan(&x) // Получаем номер ряда.
	if err != nil {
		return 0, 0, err
	}

	fmt.Print(" and column number: ")
	_, err = fmt.Scan(&y) // Получаем номер колонки.
	if err != nil {
		return 0, 0, err
	}

	return x - 1, y - 1, nil // Люди нумерируют объекты начиная с 1, но мы не люди – мы начинаем нумерацию с 0. Потому, нужно отнять 1.
}

func convertBoardToAIFormat(board [][]cellState) [][]int {
	result := make([][]int, dimension)

	for i, row := range board {
		for _, cell := range row {
			result[i] = append(result[i], int(cell))
		}
	}

	return result
}
